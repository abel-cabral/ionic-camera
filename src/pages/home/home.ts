import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  //Declaro meu objeto que vai receber o valor obtido, nessa caso starto com nada
  fotografia: any;

  constructor(
    private camera: Camera, 
    public navCtrl: NavController) {}

    foto(){
      const options: CameraOptions = {
        //Qualidade da foto e extensao da mesma
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,  
        //Agora acesso minha biblioteca buscando midias do tipo imagem      
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        //Salva a photo no dispositivo
        saveToPhotoAlbum:true
      }

      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        //Passo pra meu objeto o valor da variavel acima
        this.fotografia = base64Image;
      }, (err) => {
        // Handle error
      });
    }


    galeria(){
      const options: CameraOptions = {
        //Qualidade da foto e extensao da mesma
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }

      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        //Passo pra meu objeto o valor da variavel acima
        this.fotografia = base64Image;
      }, (err) => {
        // Handle error
      });
    }


}


